package com.liuwq.mq;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.stereotype.Component;

@Component
@EnableBinding(Sink.class)
public class MessageReceiver {

   private Logger logger = LoggerFactory.getLogger(MessageReceiver.class);
 
   @StreamListener(Sink.INPUT)
   public void process(Object message) {
        logger.info("received message : {}", message);
        logger.info("received message : {}",  new String((byte[]) message));
    }
}