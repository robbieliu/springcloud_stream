package com.liuwq.mq;

import com.liuwq.source.RabbitStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

@Component
@EnableBinding(RabbitStream.class)
public class MyMessageSender {

//    @Autowired
//    @Qualifier(RabbitStream.MY_OUTPUT)
//    private MessageChannel messageChannel;

    @Autowired
    private RabbitStream rabbitStream;

    public String send(String message) {
        MessageBuilder<String> messageBuilder = MessageBuilder.withPayload(message);
//        messageChannel.send(messageBuilder.build());
        rabbitStream.myOutput().send(messageBuilder.build());
        return "message sended : " + message;
    }

}
