package com.liuwq.mq;

import com.liuwq.source.RabbitStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Component;

@Component
@EnableBinding(RabbitStream.class)
public class TestMessageReceiver {

   private Logger logger = LoggerFactory.getLogger(TestMessageReceiver.class);
 
   @StreamListener(RabbitStream.TEST_IN_PUT)
   public void process(Object message) {
        logger.info("received test message : {}", message);
        logger.info("received test message : {}", new String((byte[]) message));
    }
}