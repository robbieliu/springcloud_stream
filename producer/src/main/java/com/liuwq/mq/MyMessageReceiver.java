package com.liuwq.mq;

import com.liuwq.source.RabbitStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Component;

@Component
@EnableBinding(RabbitStream.class)
public class MyMessageReceiver {

   private Logger logger = LoggerFactory.getLogger(MyMessageReceiver.class);
 
   @StreamListener(RabbitStream.MY_INPUT)
   public void process(Object message) {
        logger.info("received my message : {}", message);
        logger.info("received my message : {}", new String((byte[]) message));
    }
}