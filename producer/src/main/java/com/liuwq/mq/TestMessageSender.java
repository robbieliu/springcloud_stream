package com.liuwq.mq;

import com.liuwq.source.RabbitStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

@Component
@EnableBinding(RabbitStream.class)
public class TestMessageSender {

    @Autowired
    @Qualifier(RabbitStream.TEST_OUT_PUT)
    private MessageChannel messageChannel;

    public String send(String message) {
        MessageBuilder<String> messageBuilder = MessageBuilder.withPayload(message);
        messageChannel.send(messageBuilder.build());
//        rabbitStream.testOutput().send(messageBuilder.build());
        return "message sended : " + message;
    }

}
