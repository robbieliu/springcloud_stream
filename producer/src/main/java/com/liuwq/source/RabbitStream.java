package com.liuwq.source;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

public interface RabbitStream {

    String TEST_IN_PUT = "testInput";
    @Input(TEST_IN_PUT)
    SubscribableChannel testInput();

    String TEST_OUT_PUT = "testOutput";
    @Output(TEST_OUT_PUT)
    MessageChannel testOutput();



    String MY_INPUT = "myInput";
    @Input(MY_INPUT)
    SubscribableChannel myInput();

    String MY_OUTPUT = "myOutput";
    @Output(MY_OUTPUT)
    MessageChannel myOutput();
 
}
