package com.liuwq.controller;

import com.liuwq.mq.MessageSender;
import com.liuwq.mq.MyMessageSender;
import com.liuwq.mq.TestMessageSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SendMessageController {

   @Autowired
   private MessageSender messageSender;

   @GetMapping("/send")
   public Object send(String message) {
      return messageSender.send(message);
   }

   @Autowired
   private TestMessageSender testMessageSender;

   @GetMapping("/send2")
   public Object send2(String message) {
      return testMessageSender.send(message);
   }

   @Autowired
   private MyMessageSender myMessageSender;

   @GetMapping("/send3")
   public Object send3(String message) {
      return myMessageSender.send(message);
   }

}