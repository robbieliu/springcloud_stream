package com.liuwq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.cloud.stream.messaging.Source;

/**
 * Hello world!
 */
@SpringBootApplication
@EnableBinding({Source.class, Sink.class})
public class App {
    public static void main(String[] args) {

        SpringApplication.run(App.class, args);
    }
}
